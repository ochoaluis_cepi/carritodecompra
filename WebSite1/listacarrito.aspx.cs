﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listacarrito : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT au.*,a.* FROM articulos_usuarios AS au INNER JOIN articulos AS a ON au.Id_articulo=a.Id_articulo WHERE au.Id_usuario='" + Session["ID"] + "'", conexion);
        SqlDataReader car = comando.ExecuteReader();
        if (car.HasRows)
        {
            DataTable carrito = new DataTable();
            carrito.Load(car);
            Label1.Text = "";
            int count = 0;
            foreach (DataRow carr in carrito.Rows)
            {
                Label1.Text += "<tr id='datagrid-row-r1-2-" + count + "' datagrid-row-index='" + count + "' class='datagrid-row' ><td field='name'><div style='height:auto;' class='datagrid-cell datagrid-cell-c1-name'>" + carr["nombre"] + "</div></td><td field='quantity'><div style='height:auto;' class='datagrid-cell datagrid-cell-c1-quantity'>" + carr["cantidad"] + "<div></td><td field='price'><div style='height:auto;' class='datagrid-cell datagrid-cell-c1-price'>" + carr["precio"] + "<div></td></tr>";
                count++;
            }
        }
        else
        {
            Label1.Text = "<center>Sin Elementos</center>";
        }
        car.Close();
        comando.CommandText = "SELECT * FROM articulos";
        SqlDataReader result = comando.ExecuteReader();
        if (result.HasRows)
        {
            Label2.Text = "<ul class='products'>";
            while (result.Read())
            {


                Label2.Text += "<li>";
                Label2.Text += "<a href='#' class='item'>";
                Label2.Text += "<img src='Imagenes/Products/" + result["imagen"].ToString() + "' width='100' height='100px'/>";
                Label2.Text += "<div>";
                Label2.Text += "<p>" + result["nombre"] + "</p>";
                Label2.Text += "<p>Price:$" + result["precio"] + "</p>";
                Label2.Text += "</div>";
                Label2.Text += "</a>";
                Label2.Text += "</li>";


            }
            Label2.Text += "</ul>";
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE nombre LIKE '%" + TextBox1.Text + "%' ", conexion);
        SqlDataReader result = comando.ExecuteReader();
        if (result.HasRows)
        {
            Label2.Text = "<ul class='products'>";
            while (result.Read())
            {

                Label2.Text += "<li>";
                Label2.Text += "<a href='#' class='item'>";
                Label2.Text += "<img src='Imagenes/Products/" + result["imagen"].ToString() + "' width='100' height='100px'/>";
                Label2.Text += "<div>";
                Label2.Text += "<p>" + result["nombre"] + "</p>";
                Label2.Text += "<p>Price:$" + result["precio"] + "</p>";
                Label2.Text += "</div>";
                Label2.Text += "</a>";
                Label2.Text += "</li>";
            }
            Label2.Text += "</ul>";
        }
    }
}