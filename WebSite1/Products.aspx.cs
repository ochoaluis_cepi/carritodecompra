﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Products : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.QueryString["error"]!=null)
        {
            Label2.Text = "<div class='alert alert-danger' ><center>¡No Hay suficientes Existencias!</center></div>";
        }
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos ORDER BY nombre",conexion);
        SqlDataReader result = comando.ExecuteReader();
        Label1.Text = "<table align='center' class='tbl' ><tr>";
        int regla = 1;
        while(result.Read())
        {
            int exist = Convert.ToInt32(result["Existencias"]);
            if(exist<=0)
            {
                int ID = Convert.ToInt32(result["Id_articulo"]);
                Label1.Text += "<td align='center'><div id='cont" + ID + "' onmouseout='unselect(" + ID + ")' class='cont' onmouseover='select(" + ID + ")' >" + result["nombre"] + "<br> $" + result["precio"] + ".°°<br><a onclick='agrandar(" + ID + ")' ><img src='Imagenes/Products/" + result["imagen"] + "' width='100px' height='100px' ></a><br><a onclick=agrandar(" + ID + ")><img src='Imagenes/logos/carrito_vacio.png' width='30px' height='30px' title='Comprar' ></a></div></td>";
            }
            else
            {
                int ID = Convert.ToInt32(result["Id_articulo"]);
                Label1.Text += "<td align='center'><div id='cont" + ID + "' onmouseout='unselect(" + ID + ")' onmouseover='select(" + ID + ")'  class='cont' >" + result["nombre"] + "<br> $" + result["precio"] + ".°°<br><a onclick='agrandar(" + ID + ")' ><img src='Imagenes/Products/" + result["imagen"] + "' width='100px' height='100px' ></a><br><a onclick=agrandar(" + ID + ")><img src='Imagenes/logos/carrito.png' width='30px' height='30px' title='Comprar' ></a></div></td>";
            }
            if(regla%8 ==0)
            {
                Label1.Text += "</tr><tr>";
            }
            regla++;
        }
        Label1.Text += "</tr></table>";
        conexion.Close();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        string cont ="";
        if(TextBox1.Text == "todo" || TextBox1.Text=="TODO")
        {
            cont = "Select * from articulos ORDER BY nombre";
        }
        else
        {
            cont = "Select * from articulos Where nombre LIKE '%"+TextBox1.Text+"%' ORDER BY nombre";
        }
        SqlCommand comando = new SqlCommand(cont,conexion);
        SqlDataReader res = comando.ExecuteReader();
            Label1.Text = "<table align='center' class='tbl' ><tr>";
            int regla = 1;
            while (res.Read())
            {
                int exist = Convert.ToInt32(res["Existencias"]);
                if (exist <= 0)
                {
                    int ID = Convert.ToInt32(res["Id_articulo"]);
                    Label1.Text += "<td align='center'><div id='cont" + ID + "' onmouseout='unselect(" + ID + ")' onmouseover='select(" + ID + ")' class='cont' >" + res["nombre"] + "<br> $" + res["precio"] + ".°°<br><a onclick='agrandar(" + ID + ")' ><img src='Imagenes/Products/" + res["imagen"] + "' width='100px' height='100px' ></a><br><a onclick=agrandar(" + ID + ")><img src='Imagenes/logos/carrito_vacio.png' width='30px' height='30px' title='Comprar' ></a></div></td>";
                }
                else
                {
                    int ID = Convert.ToInt32(res["Id_articulo"]);
                    Label1.Text += "<td align='center'><div id='cont" + ID + "' onmouseout='unselect(" + ID + ")' onmouseover='select(" + ID + ")' class='cont' >" + res["nombre"] + "<br> $" + res["precio"] + ".°°<br><a onclick='agrandar(" + ID + ")' ><img src='Imagenes/Products/" + res["imagen"] + "' width='100px' height='100px' ></a><br><a onclick=agrandar(" + ID + ")><img src='Imagenes/logos/carrito.png' width='30px' height='30px' title='Comprar' ></a></div></td>";
                } 
                if (regla % 8 == 0)
                {
                    Label1.Text += "</tr><tr>";
                }
                regla++;
            }
            Label1.Text += "</tr></table>";
            Label1.Text += "<hr>";
            conexion.Close();
    }
}