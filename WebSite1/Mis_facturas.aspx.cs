﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mis_facturas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM facturas WHERE Id_usuario='"+Session["ID"].ToString()+"' AND activo='1'",conexion);
        SqlDataReader respuesta = comando.ExecuteReader();
        Label1.Text = "";
        while(respuesta.Read()){
            if (File.Exists(Server.MapPath(".") + "/Tickets/" + Session["ID"].ToString() + "_" + respuesta["Id_factura"].ToString() + ".txt"))
            {
                StreamReader leer = new StreamReader(Server.MapPath(".") + "/Tickets/" + Session["ID"].ToString() + "_" + respuesta["Id_factura"].ToString() + ".txt");
                Label1.Text += "<table class='table table-striped'><tr><td>";
                Label1.Text +=  leer.ReadToEnd();
                leer.Close();
                Label1.Text += "<a href='eliminar_factura.aspx?id_factura="+respuesta["Id_factura"]+"'><img src='Imagenes/logos/x.png' width='30px' height='30px'></a> <a title='Cancelar' href='Tickets/"+Session["ID"].ToString()+"_"+respuesta["Id_factura"]+".txt' download='Factura' ><img title='Descargar' src='Imagenes/logos/save.png' width='30px' height='30px' ></a>";
                Label1.Text += "</td></tr></table>";
                Label1.Text += "<hr>";
            }
        }
    }
}