﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="listacompra.aspx.cs" Inherits="listacompra" %>

  
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
<style>
    .modalmask {
    position: fixed;
    font-family: Arial, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0,0,0,0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalmask:target {
    opacity:1;
    pointer-events: auto;
}
.modalbox{
    width: 400px;
    position: relative;
    padding: 5px 20px 13px 20px;
    background: #fff;
    border-radius:3px;
    -webkit-transition: all 500ms ease-in;
    -moz-transition: all 500ms ease-in;
    transition: all 500ms ease-in;  
}
.movedown {
    margin: 0 auto;
}
.rotate {
    margin: 10% auto;
    -webkit-transform: scale(-5,-5);
    transform: scale(-5,-5);
}
.resize {
    margin: 10% auto;
    width:0;
    height:0;
}
.modalmask:target .movedown{       
    margin:10% auto;
}
.modalmask:target .rotate{     
    transform: rotate(360deg) scale(1,1);
        -webkit-transform: rotate(360deg) scale(1,1);
}
 
.modalmask:target .resize{
    width:400px;
    height:200px;
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: 1px;
    text-align: center;
    top: 1px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    border-radius:3px;
}
.close:hover {
    background: #FAAC58;
    color:#222;
}
.contenedorGeneral{
      
      width:1118px;
      height:670px;
      background-color:#D8D8D8;
      overflow:scroll;
}
  .contenedorCarrito{
      float:left;
      width:220px;
      height:100%;
      background-color:#D8D8D8;
     
  }
  .contenedorArticulos{
      float:left;
      width:220px;
      height:100%;
      background-color:#D8D8D8;
     
  }
  .elementsin{
      display:block;
      width:220px;
       font-size:12px;
      height:90px;
      background-color:#bcffb3;
  }
  .elements{
      display:block;
      font-size:12px;
      width:220px;
      height:90px;
      background-color:#c3c3fe;
     
  }
  .elementsnull{
      display:block;
      font-size:12px;
      width:220px;
      height:90px;
      background-color:#fec3c3;
      position:relative;
      
  }
  .hueco{
      display:block;
      width:220px;
      height:90px;
      background-color:#5dbf64;
      border:solid 1px #007608;
  }
  #sortable{
      width:220px;
      height:80%;
      background-color:#D8D8D8;
      float:left;
  }
  #sortable li{
      display:block;
      font-size:12px;
      width:220px;
      height:90px;
      float:left;
      background-color:#bcffb3;
  }
    .auto-style1 {
        width: 10px;
    }
    .hip:hover{
          background-color:#ffffff;
          border:solid 1px #000;
    }
    .agregado{
        font-weight:bold;
        width:200px;
        height:40px;
        border-radius:3px;
        background-color:#bcffb3;
        border:solid 1px #007608;
        position:fixed;
        bottom:25px;
        left:30px;
        font-size:12px;
        display:none
    }
    .agregado_error{
        font-weight:bold;
        width:120px;
        height:30px;
        border-radius:3px;
        background-color:#fec3c3;
        border:solid 1px #a40202;
        position:fixed;
        bottom:25px;
        left:30px;
        text-align:center;
        vertical-align:middle;
        font-size:12px;
        display:none
    }
</style>
<script>
    function agrandar(id) {
        var date = { 'ID': id }

        $.ajax({
            data: date,
            url: "modalSortable.aspx",
            type: "POST",
            beforeSend: function () {
                $("#cont").html("Procesando...");
            },
            success: function (response) {
                $("#cont").html(response);
            }
        });
        window.location = "#modal1";
    }
    
 $(function () {

     $("#sortable,#sortable2,#sortable3,#sortable4,#sortable5,#sortable6").sortable({
            connectWith: "#sortable",
            placeholder: "hueco",
            revert: true,
            opacity: 0.7,
            helper: "clone",
            receive: function (event, ui) {
                var dato = { id: ui.item.attr("id") };
                $.ajax({
                    url: "actualizar_orden.aspx",
                    method: "post",
                    data: dato,
                    success: function (msg) {
                        $(".agregado").fadeIn(1200);
                        $(".agregado").fadeOut(1200);
                    },
                    error: function (msg) {
                        $(".agregado_error").fadeIn(1200);
                        $(".agregado_error").fadeOut(1200);
                    }
                });
            }
        });
        $("#sortable2 li,#sortable3 li,#sortable4 li,#sortable5 li,#sortable6 li").draggable({
            connectToSortable:"#sortable",
            helper: "clone"
        });
 })

 $(function() {
     $('#sortable2,#sortable3,#sortable4,#sortable5 .elementsnull').sortable({disabled:true});
     $('.elementsnull').draggable('disable');
 });
 
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    
 <center><table align="center" width="500px"><tr><td><asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="nombre" DataValueField="Id_categoria">
     <asp:ListItem Value="0">Todo</asp:ListItem>
     </asp:DropDownList>
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" SelectCommand="SELECT * FROM [categoria]"></asp:SqlDataSource>
     </td><td style="vertical-align:middle">
     
     <asp:TextBox CssClass="form-control" ID="TextBox1" runat="server"></asp:TextBox></td><td style="vertical-align:middle"><asp:Button CssClass="btn btn-success" ID="Button1" runat="server" Text="Buscar" OnClick="Button1_Click" /></td></tr></table>
     
     
     </center>
     <br />
        
     <div class="contenedorGeneral">
         <asp:Label ID="Label6" runat="server"></asp:Label>
         <center>
 <asp:Label ID="Label1" runat="server"></asp:Label>
 <asp:Label ID="Label2" runat="server"></asp:Label>
         <asp:Label ID="Label3" runat="server"></asp:Label>
         <asp:Label ID="Label4" runat="server"></asp:Label>
         <asp:Label ID="Label5" runat="server"></asp:Label>
         </div>
</center>
        <div class="agregado"><center><table><tr><td style="text-align:center;vertical-align:middle">¡Elemento Agregado!</td><td style="text-align:center;vertical-align:middle"><img src="Imagenes/logos/carrito_agregado.png" width="30" height="30"/></td></tr></table></center></div>
        <div class="agregado_error">¡Error!</div>
        <div id="modal1" class="modalmask">
    <div class="modalbox movedown">
        <a href="#close" title="Close" class="close">X</a>
       <div id="cont" ></div>
    </div>
    
</div>
 </form>
</asp:Content>

