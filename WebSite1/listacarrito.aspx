﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="listacarrito.aspx.cs" Inherits="listacarrito" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="keywords" content="jquery,ui,easy,easyui,web">
	<meta name="description" content="easyui help you build your web page easily!">
	<title>jQuery EasyUI Demo</title>
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
	<style type="text/css">
		.products{
			list-style:none;
			margin-right:300px;
			padding:0px;
			height:100%;
		}
		.products li{
			display:inline;
			float:left;
			margin:10px;
		}
		.item{
			display:block;
			text-decoration:none;
		}
		.item img{
			border:1px solid #333;
		}
		.item p{
			margin:0;
			font-weight:bold;
			text-align:center;
			color:#c3c3c3;
		}
		.cart{
			position:fixed;
			right:0;
			top:220px;
			width:300px;
			height:100%;
			background:#ccc;
			padding:0px 10px;
		}
		h1{
			text-align:center;
			color:#555;
		}
		h2{
			position:absolute;
			font-size:16px;
			left:10px;
			bottom:20px;
			color:#555;
		}
		.total{
			margin:0;
			text-align:right;
			padding-right:20px;
		}
	</style>
	<script>
	    var data = { "total": 0, "rows": [] };
	    var totalCost = 0;

	    $(function () {
	        $('#cartcontent').datagrid({
	            singleSelect: true
	        });
	        $('.item').draggable({
	            revert: true,
	            proxy: 'clone',
	            onStartDrag: function () {
	                $(this).draggable('options').cursor = 'not-allowed';
	                $(this).draggable('proxy').css('z-index', 10);
	            },
	            onStopDrag: function () {
	                $(this).draggable('options').cursor = 'move';
	            }
	        });
	        $('.cart').droppable({
	            onDragEnter: function (e, source) {
	                $(source).draggable('options').cursor = 'auto';
	            },
	            onDragLeave: function (e, source) {
	                $(source).draggable('options').cursor = 'not-allowed';
	            },
	            onDrop: function (e, source) {
	                var name = $(source).find('p:eq(0)').html();
	                var price = $(source).find('p:eq(1)').html();
	                addProduct(name, parseFloat(price.split('$')[1]));
	            }
	        });
	    });

	    function addProduct(name, price) {
	        function add() {
	            for (var i = 0; i < data.total; i++) {
	                var row = data.rows[i];
	                if (row.name == name) {
	                    row.quantity += 1;
	                    return;
	                }
	            }
	            data.total += 1;
	            data.rows.push({
	                name: name,
	                quantity: 1,
	                price: price
	            });
	        }
	        add();
	        totalCost += price;
	        $('#cartcontent').datagrid('loadData', data);
	        $('div.cart .total').html('Total: $' + totalCost);
	    }
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Button" />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
    
	<div class="cart">
		<h1>Shopping Cart</h1>
		<div style="background:#fff">
		<table id="cartcontent" fitColumns="true" style="width:300px;height:auto;">
			<thead>
				<tr>
					<th field="name" width=140>Name</th>
					<th field="quantity" width=60 align="right">Quantity</th>
					<th field="price" width=60 align="right">Price</th>
				</tr>
			</thead>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
		</table>
		</div>
		<p class="total">Total: $0</p>
		
	</div>

 </form>
</asp:Content>

