﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Automatico.aspx.cs" Inherits="Carrito" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataClassesDataContext" EnableDelete="True" EnableInsert="True" EnableUpdate="True" EntityTypeName="" TableName="articulos">
        </asp:LinqDataSource>
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="Id_articulos" DataSourceID="LinqDataSource1">
            <EditItemTemplate>
                Id_articulos:
                <asp:Label ID="Id_articulosLabel1" runat="server" Text='<%# Eval("Id_articulos") %>' />
                <br />
                nombre:
                <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
                <br />
                precio:
                <asp:TextBox ID="precioTextBox" runat="server" Text='<%# Bind("precio") %>' />
                <br />
                costo:
                <asp:TextBox ID="costoTextBox" runat="server" Text='<%# Bind("costo") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            </EditItemTemplate>
            <InsertItemTemplate>
                Id_articulos:
                <asp:TextBox ID="Id_articulosTextBox" runat="server" Text='<%# Bind("Id_articulos") %>' />
                <br />
                nombre:
                <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
                <br />
                precio:
                <asp:TextBox ID="precioTextBox" runat="server" Text='<%# Bind("precio") %>' />
                <br />
                costo:
                <asp:TextBox ID="costoTextBox" runat="server" Text='<%# Bind("costo") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            </InsertItemTemplate>
            <ItemTemplate>
                Id_articulos:
                <asp:Label ID="Id_articulosLabel" runat="server" Text='<%# Eval("Id_articulos") %>' />
                <br />
                nombre:
                <asp:Label ID="nombreLabel" runat="server" Text='<%# Bind("nombre") %>' />
                <br />
                precio:
                <asp:Label ID="precioLabel" runat="server" Text='<%# Bind("precio") %>' />
                <br />
                costo:
                <asp:Label ID="costoLabel" runat="server" Text='<%# Bind("costo") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Editar" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Eliminar" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nuevo" />
            </ItemTemplate>
        </asp:FormView>
        <asp:ListView ID="ListView1" runat="server" DataKeyNames="Id_articulos" DataSourceID="LinqDataSource1">
            <AlternatingItemTemplate>
                <td runat="server" style="">Id_articulos:
                    <asp:Label ID="Id_articulosLabel" runat="server" Text='<%# Eval("Id_articulos") %>' />
                    <br />
                    nombre:
                    <asp:Label ID="nombreLabel" runat="server" Text='<%# Eval("nombre") %>' />
                    <br />
                    precio:
                    <asp:Label ID="precioLabel" runat="server" Text='<%# Eval("precio") %>' />
                    <br />
                    costo:
                    <asp:Label ID="costoLabel" runat="server" Text='<%# Eval("costo") %>' />
                    <br />
                </td>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <td runat="server" style="">Id_articulos:
                    <asp:Label ID="Id_articulosLabel1" runat="server" Text='<%# Eval("Id_articulos") %>' />
                    <br />
                    nombre:
                    <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
                    <br />
                    precio:
                    <asp:TextBox ID="precioTextBox" runat="server" Text='<%# Bind("precio") %>' />
                    <br />
                    costo:
                    <asp:TextBox ID="costoTextBox" runat="server" Text='<%# Bind("costo") %>' />
                    <br />
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Actualizar" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancelar" />
                </td>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table style="">
                    <tr>
                        <td>No se han devuelto datos.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <td runat="server" style="">Id_articulos:
                    <asp:TextBox ID="Id_articulosTextBox" runat="server" Text='<%# Bind("Id_articulos") %>' />
                    <br />nombre:
                    <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
                    <br />precio:
                    <asp:TextBox ID="precioTextBox" runat="server" Text='<%# Bind("precio") %>' />
                    <br />costo:
                    <asp:TextBox ID="costoTextBox" runat="server" Text='<%# Bind("costo") %>' />
                    <br />
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insertar" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Borrar" />
                </td>
            </InsertItemTemplate>
            <ItemTemplate>
                <td runat="server" style="">Id_articulos:
                    <asp:Label ID="Id_articulosLabel" runat="server" Text='<%# Eval("Id_articulos") %>' />
                    <br />
                    nombre:
                    <asp:Label ID="nombreLabel" runat="server" Text='<%# Eval("nombre") %>' />
                    <br />
                    precio:
                    <asp:Label ID="precioLabel" runat="server" Text='<%# Eval("precio") %>' />
                    <br />
                    costo:
                    <asp:Label ID="costoLabel" runat="server" Text='<%# Eval("costo") %>' />
                    <br />
                </td>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server" border="0" style="">
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </table>
                <div style="">
                </div>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <td runat="server" style="">Id_articulos:
                    <asp:Label ID="Id_articulosLabel" runat="server" Text='<%# Eval("Id_articulos") %>' />
                    <br />
                    nombre:
                    <asp:Label ID="nombreLabel" runat="server" Text='<%# Eval("nombre") %>' />
                    <br />
                    precio:
                    <asp:Label ID="precioLabel" runat="server" Text='<%# Eval("precio") %>' />
                    <br />
                    costo:
                    <asp:Label ID="costoLabel" runat="server" Text='<%# Eval("costo") %>' />
                    <br />
                </td>
            </SelectedItemTemplate>
        </asp:ListView>
    </form>
</body>
</html>
