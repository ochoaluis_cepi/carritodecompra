﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Carrito : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["usuario"]==null)
        {
            Response.Redirect("Login.aspx");
        }
        Label1.Text= Session["usuario"].ToString();
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("Select b.cantidad,b.Id_articulos_usuarios,a.nombre,a.precio from articulos_usuarios as b inner join articulos as a on a.id_articulo=b.id_articulo WHERE b.id_usuario = '"+Session["ID"].ToString()+"'",conexion);
        SqlDataReader result = comando.ExecuteReader();
        if(result.HasRows)
        {
            Button4.Visible = true;
            int acum = 0;
            Label2.Text = "<table class='table' border='0' align='center' style='width:700px;'><tr><td>Agregar</td><td>Quitar</td><td>cantidad</td><td>Nombre</td><td>Precio</td><td>eliminar</td></tr>";
            while(result.Read()){
                Label2.Text += "<tr><td><a href='sumar_uno.aspx?id=" + result["Id_articulos_usuarios"] + "'><img src='Imagenes/logos/mas.png' width='30px' height='30px' ></a></td><td><a href='quitar_uno.aspx?id=" + result["Id_articulos_usuarios"] + "'><img src='Imagenes/logos/menos.png' width='30px' height='30px' ></a></td><td>" + result["cantidad"] + "</td><td>" + result["nombre"].ToString() + "</td><td>$" + result["precio"] + "</td><td><a href='eliminar_carrito.aspx?id=" + result["Id_articulos_usuarios"] + "'><img src='Imagenes/logos/x.png' width='30px' height='30px' ></a></td></tr>";
                 acum +=Convert.ToInt32(result["precio"])*Convert.ToInt32(result["cantidad"]);
            }
            double subtotal = acum*0.84;
            double iva = acum * 0.16;
            Label2.Text += "<tr><td style='text-align:right' colspan='4'>Subtotal:</td><td>$" + subtotal + "</td></tr>";
            Label2.Text += "<tr><td style='text-align:right' colspan='4'>IVA:</td><td>$" + iva + "</td></tr>";
            Label2.Text += "<tr><td style='text-align:right' colspan='4'>Total:</td><td>$" + acum + "</td></tr>";
            Label2.Text += "</table>";
            HiddenField1.Value = acum.ToString();
            
        }
        else
        {
            Label2.Text = "No Existen resultados";
        }
        conexion.Close();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Login.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("Alta_Products.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand factura = new SqlCommand("INSERT INTO facturas(Id_usuario,total,activo)Values('"+Session["ID"].ToString()+"','"+HiddenField1.Value+"','1')",conexion);
        int fac = factura.ExecuteNonQuery();
        
        factura.CommandText = "SELECT MAX(Id_factura) AS fac FROM facturas";
        SqlDataReader fmax = factura.ExecuteReader();
        if(fmax.Read())
        {
            HiddenField1.Value = fmax["fac"].ToString();
            fmax.Close();
            factura.CommandText = "SELECT Id_articulo,cantidad FROM articulos_usuarios WHERE Id_usuario='"+Session["ID"]+"'";
            SqlDataReader f_a = factura.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(f_a);
            f_a.Close();
            
            foreach (DataRow r in dt.Rows)
            { 
                string id = r["Id_articulo"].ToString();
                string cant = r["cantidad"].ToString();
                factura.CommandText = "INSERT INTO facturas_art_usr(Id_factura,Id_articulo,Id_usuario,cantidad) VALUES('" + HiddenField1.Value + "','" + id + "','" + Session["ID"].ToString() + "','" + cant + "')";
                factura.ExecuteNonQuery();
                factura.CommandText = "SELECT * FROM articulos WHERE Id_articulo = '"+id+"'";
                SqlDataReader act_ex = factura.ExecuteReader();
                if(act_ex.Read())
                {
                    int ex = Convert.ToInt32(act_ex["Existencias"]) - Convert.ToInt32(cant);
                    act_ex.Close();
                    factura.CommandText = "UPDATE articulos SET Existencias='"+ex+"' WHERE Id_articulo ='"+id+"'";
                    factura.ExecuteNonQuery();
                }
            } 
            factura.CommandText = "DELETE FROM articulos_usuarios WHERE Id_usuario = '"+Session["ID"].ToString()+"'";
            factura.ExecuteNonQuery();
            factura.CommandText=" SELECT MAX(Id_factura)AS factura FROM facturas WHERE Id_usuario='"+Session["ID"].ToString()+"'";
            SqlDataReader mfac = factura.ExecuteReader();
            string nfac = "";
            if(mfac.Read())
            {
                nfac = mfac["factura"].ToString();
            }
            mfac.Close();
            factura.CommandText = "SELECT a.nombre,a.precio,f.Id_factura, fau.cantidad FROM articulos AS a INNER JOIN facturas_art_usr AS fau ON fau.Id_articulo=a.Id_articulo INNER JOIN facturas As f ON f.Id_factura=fau.Id_factura INNER JOIN usuarios AS u ON u.Id_usuario = f.Id_usuario WHERE u.Id_usuario='"+Session["ID"].ToString()+"' AND f.Id_factura='"+nfac+"'";
            SqlDataReader crearfactura = factura.ExecuteReader();
            StreamWriter arch = new StreamWriter(Server.MapPath(".")+"/Tickets/"+Session["ID"]+"_"+nfac+".txt");
            arch.WriteLine("Usuario: " + Session["usuario"] + "<br>");
            arch.WriteLine("factura: " + nfac + "<br>");
            int acum = 0;
            while(crearfactura.Read()){
                arch.WriteLine("Articulo: " + crearfactura["nombre"] + " Precio: $" + crearfactura["precio"] + " Cantidad:"+crearfactura["cantidad"]+"<br>");
                acum += Convert.ToInt32(crearfactura["precio"]) * Convert.ToInt32(crearfactura["cantidad"]); 
            }
            double subtotal = acum * 0.84;
            double iva = acum * 0.16;
            arch.WriteLine("Subtotal:$" + subtotal + "<br>");
            arch.WriteLine("IVA:$" + iva + "<br>");
            arch.WriteLine("Total:$" + acum + "<br>");
            arch.Close();
            Response.AddHeader("REFRESH","2;URL=Carrito.aspx");
        }else{
            Label3.Text = "sin contenido";
        } 
        conexion.Close();
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mis_facturas.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        Response.Redirect("Servicio_consulta.aspx");
    }
}