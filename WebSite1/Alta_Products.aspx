﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Alta_Products.aspx.cs" Inherits="Alta_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <center>
    <table class="table" align="center"  style="width:307px;">
        <tr>
            <td class="auto-style1">Nombre</td>
            <td>
                <asp:TextBox class="form-control" required ID="TextBox1" runat="server"></asp:TextBox>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="Solo Letras" ValidationGroup="[a-zA-Z]"><div class="alert alert-danger">Solo Letras</div> </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Proveedor</td>
            <td>
                <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="nombre" DataValueField="Id_proveedor">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" SelectCommand="SELECT [Id_proveedor], [nombre] FROM [proveedores]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Categoria</td>
            <td>
                <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="Id_categoria">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" SelectCommand="SELECT [Id_categoria], [nombre] FROM [categoria]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Precio</td>
            <td>
                <asp:TextBox class="form-control" required ID="TextBox2" runat="server" OnTextChanged="TextBox2_TextChanged" ValidateRequestMode="Enabled"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="Solo Numeros" ValidationGroup="[0-9]" Display="Dynamic"><div class="alert alert-danger">Solo Numeros</div></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Costo:</td>
            <td>
                <asp:TextBox class="form-control" required ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox3" ErrorMessage="Solo Numeros" ValidationGroup="[0-9]" Display="Dynamic"><div class="alert alert-danger">Solo Numeros</div></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Imagen:</td>
            <td>
                <asp:FileUpload class="form-control" ID="FileUpload1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align:center"  colspan="2">
                <asp:Button ID="Button1" class="btn btn-lg btn-primary btn-block" runat="server" Text="Registrar" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>
       <center> <asp:Label ID="Label1" runat="server"></asp:Label></center>
    </center>
        </form>
   </asp:Content>
