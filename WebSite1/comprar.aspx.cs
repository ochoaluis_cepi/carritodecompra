﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class comprar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ID"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        int cantidad = Convert.ToInt32(Request.QueryString["cantidad"]);
        int id_articulo = Convert.ToInt32(Request.QueryString["id_articulo"]);
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand cmd = new SqlCommand("SELECT * FROM articulos_usuarios WHERE Id_articulo='"+id_articulo+"' AND Id_usuario='"+Session["ID"]+"'",conexion);
        SqlDataReader res = cmd.ExecuteReader();
        if(res.Read())
        {
                int acum = Convert.ToInt32(res["cantidad"]) + cantidad;
                res.Close();
                cmd.CommandText = "UPDATE articulos_usuarios SET cantidad='" + acum + "' WHERE id_usuario='" + Session["ID"] + "' AND Id_articulo='" + id_articulo + "'";
                cmd.ExecuteNonQuery();
        }
        else
        {
            res.Close();
            cmd.CommandText = "INSERT INTO articulos_usuarios (Id_articulo,Id_usuario,cantidad) VALUES ('" + id_articulo + "','" + Session["ID"] + "','" + cantidad + "')";
            cmd.ExecuteNonQuery();
        }
        if(Request.QueryString["opc"]!=null)
        {
            Response.Redirect("listacompra.aspx");
        }
        else
        {
            Response.Redirect("Products.aspx");
        }
       
       
        conexion.Close();
    }
}