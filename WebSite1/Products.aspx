﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
        <script >
            function agrandar(id)
            {
                var date = { 'ID': id }
              
                $.ajax({
                    data: date,
                    url: "modal.aspx",
                    type: "POST",
                    beforeSend: function () {
                        $("#cont").html("Procesando...");
                    },
                    success: function (response) {
                        $("#cont").html(response);
                    }
                });
                window.location = "#modal1";
            }
            function select(id)
            {
                document.getElementById("cont" + id).style.borderColor = "#505050";
                document.getElementById("cont" + id).style.background = "#b6ddff";
            }
            function unselect(id)
            {
                document.getElementById("cont" + id).style.borderColor = "#808080";
                document.getElementById("cont" + id).style.background = "#f2f2f2";
            }
        </script>
        <style>
            .modalmask {
    position: fixed;
    font-family: Arial, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0,0,0,0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalmask:target {
    opacity:1;
    pointer-events: auto;
}
.modalbox{
    width: 400px;
    position: relative;
    padding: 5px 20px 13px 20px;
    background: #fff;
    border-radius:3px;
    -webkit-transition: all 500ms ease-in;
    -moz-transition: all 500ms ease-in;
    transition: all 500ms ease-in;  
}
.movedown {
    margin: 0 auto;
}
.rotate {
    margin: 10% auto;
    -webkit-transform: scale(-5,-5);
    transform: scale(-5,-5);
}
.resize {
    margin: 10% auto;
    width:0;
    height:0;
}
.modalmask:target .movedown{       
    margin:10% auto;
}
.modalmask:target .rotate{     
    transform: rotate(360deg) scale(1,1);
        -webkit-transform: rotate(360deg) scale(1,1);
}
 
.modalmask:target .resize{
    width:400px;
    height:200px;
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: 1px;
    text-align: center;
    top: 1px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    border-radius:3px;
}
.close:hover {
    background: #FAAC58;
    color:#222;
}
        </style>
    <div>
        <br />
        <center><asp:TextBox ID="TextBox1" required CssClass="form-control" placeholder="Buscar Producto"  style="width:250px;" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" class="btn btn-lg btn-primary btn-block" style="width:250px;" OnClick="Button1_Click" Text="Buscar" />
        </center><br />
     <br />
    </div>
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>
<div id="modal1" class="modalmask">
    <div class="modalbox movedown">
        <a href="#close" title="Close" class="close">X</a>
       <div id="cont" ></div>
    </div>
    
</div>
            </center>
    </form>
</asp:Content>
