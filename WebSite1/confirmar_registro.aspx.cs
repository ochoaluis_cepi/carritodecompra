﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class confirmar_registro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ID = Request.QueryString["id_usuario"];
        string token = Request.QueryString["token"];

        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT u.Id_usuario,t.token,u.nombre FROM usuarios AS u INNER JOIN token AS t ON u.Id_usuario = t.Id_usuario WHERE u.Id_usuario='"+ID+"' AND t.token='"+token+"'",conexion);
        SqlDataReader result = comando.ExecuteReader();
        if(result.Read())
        {
            string id_usr = result["Id_usuario"].ToString();
            string nombre = result["nombre"].ToString();
            string id_tk = result["token"].ToString();
            result.Close();
            comando.CommandText = "UPDATE usuarios SET activo='1' WHERE Id_usuario = '"+id_usr+"'";
            int act_usr = comando.ExecuteNonQuery();
            if(act_usr ==1)
            {
                comando.CommandText = "DELETE FROM token WHERE token='"+id_tk+"' AND Id_usuario='"+id_usr+"'";
                int del_tk = comando.ExecuteNonQuery();
                if(del_tk==1)
                {
                    Session["usuario"] = nombre;
                    Session["ID"] = id_usr;
                    Response.Redirect("Carrito.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
}