﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Proveedores.aspx.cs" Inherits="Proveedores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
        <p>
            <asp:FormView ID="FormView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" DataKeyNames="Id_proveedor" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Both">
                <EditItemTemplate>
                    ID:
                    <asp:Label ID="Id_proveedorLabel1" runat="server" Text='<%# Eval("Id_proveedor") %>' />
                    <br />
                    Nombre:
                    <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
                    <br />
                    Empresa:
                    <asp:TextBox ID="nombre_empresaTextBox" runat="server" Text='<%# Bind("nombre_empresa") %>' />
                    <br />
                    Telefono:
                    <asp:TextBox ID="telefonoTextBox" runat="server" Text='<%# Bind("telefono") %>' />
                    <br />
                    Direccion:
                    <asp:TextBox ID="direccionTextBox" runat="server" Text='<%# Bind("direccion") %>' />
                    <br />
                    Estado:
                    <asp:TextBox ID="estadoTextBox" runat="server" Text='<%# Bind("estado") %>' />
                    <br />
                    Municipio:
                    <asp:TextBox ID="municipioTextBox" runat="server" Text='<%# Bind("municipio") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                </EditItemTemplate>
                
                <InsertItemTemplate>
                    <table class="table" style="width:100%;">
                        <tr>
                            <td>Nombre:</td>
                            <td><asp:TextBox CssClass="form-control" ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' /></td>
                        </tr>
                        <tr>
                            <td>Empresa:</td>
                            <td><asp:TextBox CssClass="form-control" ID="nombre_empresaTextBox" runat="server" Text='<%# Bind("nombre_empresa") %>' /> </td>
                        </tr>
                        <tr>
                            <td>Telefono:</td>
                            <td><asp:TextBox CssClass="form-control" ID="telefonoTextBox" runat="server" Text='<%# Bind("telefono") %>' /></td>
                        </tr>
                        <tr>
                            <td>Direccion:</td>
                            <td><asp:TextBox CssClass="form-control" ID="direccionTextBox" runat="server" Text='<%# Bind("direccion") %>' /></td>
                        </tr>
                        <tr>
                            <td>Estado:</td>
                            <td><asp:TextBox CssClass="form-control" ID="estadoTextBox" runat="server" Text='<%# Bind("estado") %>' />
                        </tr>
                        <tr>
                            <td>Municipio:</td>
                            <td><asp:TextBox CssClass="form-control" ID="municipioTextBox" runat="server" Text='<%# Bind("municipio") %>' />
                        </tr>
                        <tr>

                            <td colspan="2"><asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                        </tr>
                    </table>
                    
                    
                </InsertItemTemplate>
                <ItemTemplate>
                    Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="nombreLabel" runat="server" Text='<%# Bind("nombre") %>' />
                    <br />
                    Empresa:&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="nombre_empresaLabel" runat="server" Text='<%# Bind("nombre_empresa") %>' />
                    <br />
                    Telefono:&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="telefonoLabel" runat="server" Text='<%# Bind("telefono") %>' />
                    <br />
                    Direccion:&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="direccionLabel" runat="server" Text='<%# Bind("direccion") %>' />
                    <br />
                    Estado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="estadoLabel" runat="server" Text='<%# Bind("estado") %>' />
                    <br />
                    Municipio:&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="municipioLabel" runat="server" Text='<%# Bind("municipio") %>' />
                    <br />
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nuevo" />
                </ItemTemplate>
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
            </asp:FormView>
            <br />
        </p>
        <p>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id_proveedor" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id_proveedor" HeaderText="   ID   " InsertVisible="False" ReadOnly="True" SortExpression="Id_proveedor" />
                    <asp:BoundField DataField="nombre" HeaderText="   Nombre   " SortExpression="nombre" />
                    <asp:BoundField DataField="nombre_empresa" HeaderText="   Empresa   " SortExpression="nombre_empresa" />
                    <asp:BoundField DataField="telefono" HeaderText="   Telefono   " SortExpression="telefono" />
                    <asp:BoundField DataField="direccion" HeaderText="   Direccion   " SortExpression="direccion" />
                    <asp:BoundField DataField="estado" HeaderText="   Estado   " SortExpression="estado" />
                    <asp:BoundField DataField="municipio" HeaderText="   Municipio   " SortExpression="municipio" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </p>
        <p>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" DeleteCommand="DELETE FROM [proveedores] WHERE [Id_proveedor] = @Id_proveedor" InsertCommand="INSERT INTO [proveedores] ([nombre], [nombre_empresa], [telefono], [direccion], [estado], [municipio]) VALUES (@nombre, @nombre_empresa, @telefono, @direccion, @estado, @municipio)" SelectCommand="SELECT * FROM [proveedores]" UpdateCommand="UPDATE [proveedores] SET [nombre] = @nombre, [nombre_empresa] = @nombre_empresa, [telefono] = @telefono, [direccion] = @direccion, [estado] = @estado, [municipio] = @municipio WHERE [Id_proveedor] = @Id_proveedor">
                <DeleteParameters>
                    <asp:Parameter Name="Id_proveedor" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="nombre" Type="String" />
                    <asp:Parameter Name="nombre_empresa" Type="String" />
                    <asp:Parameter Name="telefono" Type="String" />
                    <asp:Parameter Name="direccion" Type="String" />
                    <asp:Parameter Name="estado" Type="String" />
                    <asp:Parameter Name="municipio" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="nombre" Type="String" />
                    <asp:Parameter Name="nombre_empresa" Type="String" />
                    <asp:Parameter Name="telefono" Type="String" />
                    <asp:Parameter Name="direccion" Type="String" />
                    <asp:Parameter Name="estado" Type="String" />
                    <asp:Parameter Name="municipio" Type="String" />
                    <asp:Parameter Name="Id_proveedor" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </p>
            </center>
    </form>
</asp:Content>

