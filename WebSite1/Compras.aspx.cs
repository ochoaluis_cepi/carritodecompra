﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

public partial class Compras : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.QueryString["error"]!=null)
        {
            int req = Convert.ToInt32(Request.QueryString["error"]);
            switch(req){
                case '1':
                    Label3.Text = "<div class='alert alert-danger' ><center>¡ERROR! No se pudo leer el articulo</center></div>";
                    break;
                case '2':
                    Label3.Text = "<div class='alert alert-danger' ><center>¡ERROR! No se pudo eliminar el articulo</center></div>";
                    break;
            }
            
        }

        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos",conexion);
        SqlDataReader result = comando.ExecuteReader();
        Label1.Text = "";
        int cont = 1;
        if(result.HasRows)
        {
            Label1.Text += "<table class='tbl'>";
            Label1.Text += "<tr>";
            while(result.Read())
            {
                Label1.Text += "<td><div class='cont'>"+result["nombre"]+"<br><a href='reg_comp.aspx?id_articulo="+result["Id_articulo"]+"' ><img src='Imagenes/Products/"+result["imagen"]+"' width='100px' height='100px' ></a></div></td>";
                if(cont%8==0)
                {
                    Label1.Text += "</tr><tr>";
                }
                cont++;
            }
            Label1.Text += "</tr></table>";

        }
        result.Close();
        comando.CommandText = "SELECT ct.*,a.nombre As art FROM compras_temporal AS ct INNER JOIN articulos AS a ON a.Id_articulo=ct.Id_articulo WHERE Id_usuario = '"+Session["ID"]+"'";
        SqlDataReader res = comando.ExecuteReader();
        double tot = 0;
        if (res.HasRows)
        {
            Label2.Text = "<table class='table' width='500px' >";
            Label2.Text += "<tr><td>ID</td><td>Nombre</td><td>Costo</td><td>Existencias a Ingresar</td><td>Monto</td><td>Eliminar</td></tr>";
            while(res.Read()){
                double monto = Convert.ToDouble(res["costo"]) * Convert.ToDouble(res["existencias"]);
                Label2.Text += "<tr><td>"+res["Id_compras_temporal"]+"</td><td>"+res["art"]+"</td><td>$"+res["costo"]+"</td><td>"+res["existencias"]+"</td><td>$"+monto+"</td><td><a href='eliminar_compra.aspx?id_compras_temporal="+res["Id_compras_temporal"]+"' ><img src='Imagenes/logos/x.png' width='50px' heigth='30px' ></a></td></tr>";
                tot += monto;
            }
            double subt = tot * 0.84;
            double iva = tot * 0.16;
            Label2.Text += "<tr><td colspan='4' align='right' >Sub Total:</td><td>$" + subt + "</td></tr>";
            Label2.Text += "<tr><td colspan='4' align='right' >IVA:</td><td>$" + iva + "</td></tr>";
            Label2.Text += "<tr><td colspan='4' align='right' >Total:</td><td>$"+tot+"</td></tr>";
            Label2.Text +="</table>";
            HiddenField1.Value = tot.ToString();
            Button2.Visible = true;
            DropDownList1.Visible = true;
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE nombre LIKE '%"+TextBox1.Text+"%'",conexion);
        SqlDataReader result = comando.ExecuteReader();
        Label1.Text = "";
        int cont = 1;
        if (result.HasRows)
        {
            Label1.Text += "<table class='tbl'>";
            Label1.Text += "<tr>";
            while (result.Read())
            {
                Label1.Text += "<td><div class='cont'>" + result["nombre"] + "<br><a href='reg_comp.aspx?id_articulo=" + result["Id_articulo"] + "' ><img src='Imagenes/Products/" + result["imagen"] + "' width='100px' height='100px' ></a></div></td>";
                if (cont % 8 == 0)
                {
                    Label1.Text += "</tr><tr>";
                }
                cont++;
            }
            Label1.Text += "</tr></table>";

        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("INSERT INTO compras(Id_proveedor,monto)VALUES('"+DropDownList1.SelectedValue+"','"+HiddenField1.Value+"')",conexion);
        int result = comando.ExecuteNonQuery();
        if(result ==1)
        {
            comando.CommandText = "SELECT MAX(Id_compra) as max FROM compras";
            SqlDataReader max = comando.ExecuteReader();
            if(max.Read())
            {
                string Id_compra = max["max"].ToString();
                max.Close();
                comando.CommandText = "SELECT * FROM compras_temporal WHERE Id_usuario = '"+Session["ID"]+"'";
                SqlDataReader registros_temporales = comando.ExecuteReader();
                if(registros_temporales.HasRows)
                {
                    DataTable dtb = new DataTable();
                    dtb.Load(registros_temporales);
                    registros_temporales.Close();
                    foreach(DataRow reg in dtb.Rows)
                    {
                        comando.CommandText = "INSERT INTO compras_articulos(Id_compra,Id_articulo,costo,existencias)VALUES('"+Id_compra+"','"+reg["Id_articulo"]+"','"+reg["costo"]+"','"+reg["existencias"]+"')";
                        comando.ExecuteNonQuery();
                        comando.CommandText = "SELECT * FROM articulos WHERE Id_articulo='"+reg["Id_articulo"]+"'";
                        SqlDataReader actualizar=comando.ExecuteReader();
                        DataTable act = new DataTable();
                        act.Load(actualizar);
                        actualizar.Close();
                        foreach(DataRow ex in act.Rows)
                        {
                            int ID =Convert.ToInt32(ex["Id_articulo"]);
                            int existencias = Convert.ToInt32(ex["Existencias"]) + Convert.ToInt32(reg["existencias"]);
                            comando.CommandText = "UPDATE articulos SET costo='"+reg["costo"]+"', Existencias='"+existencias+"' WHERE Id_articulo='"+ID+"'";
                            int fin = comando.ExecuteNonQuery();
                            if(fin ==1)
                            {
                                Label4.Text = "Alta Correcta";
                            }
                        }
                    }
                    comando.CommandText = "DELETE FROM compras_temporal WHERE Id_usuario='"+Session["ID"]+"'";
                    comando.ExecuteNonQuery();
                }
            }
        }
    }
}