﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/personal.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="scripts/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style2 {
            width: 109px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="login" >
        <br />
       
    <table class="table" align="center" style="width:265px;">
        <tr>
            <td class="auto-style2" >Usuario:</td>
            <td>
                <asp:TextBox ID="TextBox1" CssClass="form-control" required runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>

            <td class="auto-style2" >Contraseña:</td>
            <td>
                <asp:TextBox ID="TextBox2" CssClass="form-control"  required runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <asp:Button class="btn btn-lg btn-primary btn-block" ID="Button1" runat="server" OnClick="Button1_Click" Text="Login" />
                <br />
            </td>
        </tr>
        <tr>
            <td style="text-align:left" >
                <asp:HyperLink ID="HyperLink1" Font-Size="10px" runat="server" NavigateUrl="~/Registro_usuarios.aspx">Registro</asp:HyperLink>
            </td>
            <td style="text-align:right" >
                <asp:HyperLink ID="HyperLink2" Font-Size="10px" runat="server" NavigateUrl="~/recuperar_contrasena.aspx">recuperar Contraseña</asp:HyperLink>
            </td>
        </tr>
    </table>
    </div>
        <center><asp:Label ID="Label1" runat="server"></asp:Label></center>
    </form>
  
</body>
</html>
