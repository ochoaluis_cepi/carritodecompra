﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class permisos_usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id_user = Request.QueryString["id"];
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM permisos",conexion);
        SqlDataReader result = comando.ExecuteReader();
        if(result.HasRows)
        {
            DataTable permisos = new DataTable();
            permisos.Load(result);
            result.Close();
            Label1.Text = "<center>Administrar Permisos<br><table class='table' width='500px' align='center'><tr><td>Permiso</td><td>S/N</td></tr>";
            foreach(DataRow perm in permisos.Rows)
            {
                Label1.Text += "<tr><td>" + perm["nombre"] + "</td><td>";
              
                comando.CommandText = "SELECT * FROM usuarios_permisos WHERE Id_usuario='"+id_user+"' and Id_permiso='"+perm["Id_permiso"]+"'";
                
                SqlDataReader p = comando.ExecuteReader();
                if(p.Read())
                {
                    Label1.Text += "<input id='si"+perm["Id_permiso"]+"' onclick='asign_permiso("+perm["Id_permiso"]+","+id_user+")' type='checkbox' checked='checked'>";
                }
                else
                {
                    Label1.Text += "<input id='no" + perm["Id_permiso"] + "' onclick='asign_permiso(" + perm["Id_permiso"] + "," + id_user + ")' type='checkbox'>";
                }
                p.Close();

                Label1.Text +="</td></tr>";
            }
            Label1.Text += "</table></center>";


        }
        

    }
}