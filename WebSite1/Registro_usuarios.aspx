﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registro_usuarios.aspx.cs" Inherits="Registro_usuarios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/personal.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="scripts/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 182px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="login" style="width:400px">
    
        <table style="width:100%;">
            <tr>
                <td colspan="2" class="auto-style1"><h2>Nuevo registro</h2></td>   
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">
                    <br />
                    Nombre:</td>
                <td>
                    <br />
                    <asp:TextBox ID="TextBox1"  CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="&lt;div class='alert alert-danger'&gt;Nombre Incorrecto&lt;/div&gt;" ValidationExpression="^[a-zA-Z_áéíóúñ\s]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">direccion:</td>
                <td>
                    <asp:TextBox ID="TextBox2"  CssClass="form-control" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">Telefono</td>
                <td>
                    <asp:TextBox ID="TextBox3"  CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox3" Display="Dynamic" ErrorMessage="&lt;div class='alert alert-danger'&gt;solo numero telefonico&lt;/div&gt;" ValidationExpression="([0-9]|-)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">Correo:</td>
                <td>
                    <asp:TextBox ID="TextBox4"  CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBox4" Display="Dynamic" ErrorMessage="&lt;div class='alert alert-danger'&gt;E Mail Invalido&lt;/div&gt;" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">Usuario:</td>
                <td>
                    <asp:TextBox ID="TextBox5"  CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBox5" Display="Dynamic" ErrorMessage="Formato Incorrecto" ValidationExpression="[A-Za-z/0-9]*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:left"  class="auto-style1">Contraseña:</td>
                <td>
                    <asp:TextBox ID="TextBox6" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">Repite la Contraseña:</td>
                <td>
                    <asp:TextBox ID="TextBox7"  CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox6" ControlToValidate="TextBox7" Display="Dynamic" ErrorMessage="&lt;div class='alert alert-danger'&gt;Contraseñas Diferentes&lt;/div&gt;"></asp:CompareValidator>
               
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBox6" Display="Dynamic" ErrorMessage="&lt;div class='alert alert-danger'&gt;No Comple especificaciones&lt;/div&gt;" ValidationExpression="^[\Wa-zA-Z0-9]{8,15}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" class="auto-style1">Fecha de Nacimiento:</td>
                <td>
                    <asp:TextBox ID="TextBox8" CssClass="form-control" runat="server"  TextMode="Date" OnTextChanged="TextBox8_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align:left" >
                    <br />
                    <asp:Button ID="Button1" class="btn btn-success"  runat="server" Text="Registrar" OnClick="Button1_Click" />
                    <br />
                    <br />
                </td>
                 <td style="text-align:right">
                     <br />
                    <asp:Button ID="Button2" class="btn btn-danger"  runat="server" Text="Cancelar" OnClick="Button2_Click" PostBackUrl="~/Login.aspx" />
                     <br />
                     <br />
                </td>
            </tr>
        </table>
    
    </div>
        <p>
&nbsp;&nbsp;
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>
