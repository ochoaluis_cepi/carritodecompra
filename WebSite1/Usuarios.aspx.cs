﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["usuario"] == null)
        {
            Response.Redirect("Login.aspx");
        }
       
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("Select * FROM usuarios WHERE id_usuario>1", conexion);
        SqlDataReader result = comando.ExecuteReader();
        if (result.HasRows)
        {
            Label2.Text = "<table class='table table-striped' border='0' align='center' style='width:800px;'><tr><td>ID</td><td>Nombre</td><td>Direccion</td><td>telefono</td><td>Correo</td><td>Usuario</td><td>Fecha de Nacimiento</td><td>eliminar</td><td>Permisos</td></tr>";
            while (result.Read())
            {
                Label2.Text += "<tr><td>" + result["Id_usuario"] + "</td><td>" + result["nombre"].ToString() + "</td><td>" + result["direccion"].ToString() + "</td><td>" + result["telefono"].ToString() + "</td><td>$" + result["correo"] + "</td><td>" + result["usuario"].ToString() + "</td><td>" + result["fecha_nacimiento"].ToString() + "</td><td><a href='eliminar_usuario.aspx?id=" + result["Id_usuario"] + "'><img src='Imagenes/logos/x.png' width='40px' height='40px' ></a></td><td><a href='permisos_usuarios.aspx?id=" + result["Id_usuario"] + "'><img src='Imagenes/logos/permiso.png' width='40px' height='40px' ></a></td></tr>";   
            }
            Label2.Text += "</table>";
        }
        else
        {
            Label2.Text = "No Existen resultados";
        }
        conexion.Close();
    }
}