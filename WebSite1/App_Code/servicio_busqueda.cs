﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Descripción breve de servicio_busqueda
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class servicio_busqueda : System.Web.Services.WebService {

    public servicio_busqueda () {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string consulta(string nombre) {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE nombre = '"+nombre+"'",conexion);
        SqlDataReader result = comando.ExecuteReader();
        if(result.Read())
        {
            return "Nombre: "+result["nombre"].ToString()+"ID: "+result["Id_articulo"].ToString();
        }
        else
        {
            return "No Existe el Registro";
        }

    }
    
}
