﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Data;

/// <summary>
/// Descripción breve de conexion
/// </summary>
public class conexion
{
	public conexion()
	{
      
	}
    public DataTable consulta(string consult)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand(consult, conexion);
        SqlDataReader res = comando.ExecuteReader();
        DataTable tbl = new DataTable();
        tbl.Load(res);
        return tbl;
    }
}