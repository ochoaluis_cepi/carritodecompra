﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Permisos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["ID"]!=null)
        {
            string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            SqlConnection conexion = new SqlConnection(s);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT p.* FROM permisos AS p INNER JOIN usuarios_permisos AS up ON up.Id_permiso=p.Id_permiso WHERE up.Id_usuario = '"+Session["ID"]+"'",conexion);
            SqlDataReader result = comando.ExecuteReader();
            Label1.Text = "";
            if(result.HasRows)
            {
                Label1.Text += "<table align='center' class='table' style='width:500px;text-align:center' >";
                Label1.Text +="<tr><td>ID</td><td>Nombre</td><td>Permiso</td></tr>";
                while(result.Read()){
                    Label1.Text += "<tr><td>"+result["Id_permiso"]+"</td><td>"+result["nombre"]+"</td><td><img src='Imagenes/logos/ok.png' width='50px' height='50px'></td></tr>";
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
}