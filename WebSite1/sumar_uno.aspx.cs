﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sumar_uno : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            bool bandera = false;
            string ID = Request.QueryString["id"].ToString();
            if(Request.QueryString["opc"]!=null)
            {
                bandera = true;
            }
            string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            SqlConnection conexion = new SqlConnection(s);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT au.*,a.Existencias FROM articulos_usuarios AS au INNER JOIN articulos AS a ON au.Id_articulo=a.Id_articulo WHERE Id_articulos_usuarios='"+ID+"'",conexion);
            SqlDataReader result = comando.ExecuteReader();
            if(result.Read())
            {
                int cant =Convert.ToInt32( result["cantidad"]);
                int existencias = Convert.ToInt32(result["Existencias"]);
                cant++;
                if(existencias<cant)
                {
                    if(bandera)
                    {
                        Response.Redirect("listacompra.aspx");
                    }
                    Response.Redirect("Carrito.aspx");
                }
                else
                {
                    result.Close();
                    comando.CommandText = "UPDATE articulos_usuarios SET cantidad='"+cant+"' WHERE Id_articulos_usuarios='"+ID+"'";
                    comando.ExecuteNonQuery();
                    if (bandera)
                    {
                        Response.Redirect("listacompra.aspx");
                    }
                    Response.Redirect("Carrito.aspx");

                }
                
            }
        }
        else
        {
            Response.Redirect("Carrito.aspx");
        }
        
    }
}