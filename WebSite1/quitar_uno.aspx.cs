﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class quitar_uno : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"]!=null)
        {
            bool bandera = false;
            if(Request.QueryString["opc"]!=null)
            {
                bandera = true;
            }
            string ID = Request.QueryString["id"];
            string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            SqlConnection conexion = new SqlConnection(s);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT * FROM articulos_usuarios WHERE Id_articulos_usuarios = '"+ID+"'",conexion);
            SqlDataReader result = comando.ExecuteReader();
            if(result.Read())
            {
                int num = Convert.ToInt32(result["cantidad"]);
                if(num == 1)
                {
                    Response.Redirect("Carrito.aspx");
                }
                else
                {
                    num--;
                    result.Close();
                    comando.CommandText = "UPDATE articulos_usuarios SET cantidad='"+num+"' WHERE Id_articulos_usuarios='"+ID+"'";
                    comando.ExecuteNonQuery();
                    if (bandera)
                    {
                        Response.Redirect("listacompra.aspx");
                    }
                    Response.Redirect("Carrito.aspx");
                }
            }
            else
            {
                if (bandera)
                {
                    Response.Redirect("listacompra.aspx");
                }
                Response.Redirect("Carrito.aspx");
            }

            conexion.Close();
        }
        else
        {
            
            Response.Redirect("Carrito.aspx");
        }

    }
}