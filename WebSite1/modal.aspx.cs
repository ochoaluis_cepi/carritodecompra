﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ID = Request.Form["ID"];
        
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE Id_articulo='"+ID+"'",conexion);
        SqlDataReader result = comando.ExecuteReader();
        Label1.Text = "";
        if(result.Read())
        {
            TextBox2.Text = result["Id_articulo"].ToString();
            Label4.Text = "<img src='Imagenes/Products/"+result["imagen"]+"' width='150px' heigth='150px' >";
            Label1.Text = result["nombre"].ToString();
            Label2.Text = result["precio"].ToString();
            Label3.Text = result["Existencias"].ToString();
            
            int exist = Convert.ToInt32(result["Existencias"]);
            TextBox3.Text = exist.ToString();
            if(exist<=0)
            {
            ImageButton1.Visible = false ;
            TextBox1.Visible = false;
            }
            else
            {
            ImageButton1.Visible = true;
            }
       
        }
        else
        {
            Label1.Text = "¡Error! No Existe el Articulo";
        }
        conexion.Close();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        int existencia = Convert.ToInt32(TextBox3.Text);
        int compra = Convert.ToInt32(TextBox1.Text);
        if (existencia < compra)
        {
            Response.Redirect("Products.aspx?error=1");
        }
        else
        {
            Response.Redirect("comprar.aspx?id_articulo=" + TextBox2.Text + "&cantidad=" + TextBox1.Text);
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
}