﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listacompra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT au.*,a.* FROM articulos_usuarios AS au INNER JOIN articulos AS a ON au.Id_articulo=a.Id_articulo WHERE au.Id_usuario='" + Session["ID"] + "' ORDER BY a.nombre", conexion);
        SqlDataReader car = comando.ExecuteReader();
        if (car.HasRows)
        {
            DataTable carrito = new DataTable();
            carrito.Load(car);
            Label1.Text = "<div  id='sortable'>";
            Label6.Text = "Total:$";
            int acum = 0;
            foreach (DataRow carr in carrito.Rows)
            {
               
                acum += Convert.ToInt32(carr["cantidad"]) * Convert.ToInt32(carr["precio"]);
                Label1.Text += "<li id='" + carr["Id_articulo"] + "' class='elementsin' >";
                Label1.Text += "<table width='95%' ><tr><td align='right' style='line-height:3px' colspan='3' >";
                Label1.Text += "<a href='sumar_uno.aspx?id=" + carr["Id_articulos_usuarios"] + "&opc=2'><img title='Agregar' class='hip' src='Imagenes/logos/mas.png' width='10px' height='10px' ></a>                                      ";
                Label1.Text += "<a href='quitar_uno.aspx?id=" + carr["Id_articulos_usuarios"] + "&opc=2'><img title='Quitar' src='Imagenes/logos/menos.png' class='hip' width='10px' height='10px' ></a>                                       ";
                Label1.Text += "<a href='eliminar_carrito.aspx?id=" + carr["Id_articulos_usuarios"] + "&opc=2'><img title='Eliminar' src='Imagenes/logos/x.png' class='hip' width='10px' height='10px' ></a>";
                Label1.Text += "</td><tr><tr><td rowspan='4' style='vertical-align:middle;width:50px'><a onclick='agrandar(" + carr["Id_articulo"] + ")'><img src='Imagenes/Products/" + carr["imagen"].ToString() + "' width='50px' heigth='50px'></a></td></tr><tr><td style='width:60px'>Nombre:</td><td>" + carr["nombre"] + "</td></tr><tr><td>Cantidad:</td><td>" + carr["cantidad"] + "<input type='hidden' id='cantidad" + carr["Id_articulo"] + "' value='" + carr["cantidad"] + "'><input type='hidden' id='precio" + carr["Id_articulo"] + "' value='" + carr["precio"] + "'></td></tr><tr><td>Precio:</td><td>$" + carr["precio"] + "</td></tr></table></li>";
            }
            Label1.Text += "</div>";
            Label6.Text += acum+"°°";
        }
        else
        {
            Label1.Text = "<div  id='sortable'  >Mi Carrito  ";
            Label1.Text += "</div>";
        }
        car.Close();
        comando.CommandText = "SELECT * FROM articulos ORDER BY nombre";
        SqlDataReader articulos = comando.ExecuteReader();
        if(articulos.HasRows){
            Label2.Text = "<div id='sortable2' class='contenedorArticulos' >";
            Label3.Text = "<div id='sortable3' class='contenedorArticulos' >";
            Label4.Text = "<div id='sortable4' class='contenedorArticulos' >";
            Label5.Text = "<div id='sortable5' class='contenedorArticulos' >";
            int controlador = '0';
            while(articulos.Read())
            {
                int ex = Convert.ToInt32(articulos["Existencias"]);
                if(ex<=0)
                {
                    switch(controlador)
                    {
                        case '0':
                            Label2.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td style='width:60px' align='left'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                        break;
                        case '1':
                            Label3.Text += "<li id='" + articulos["Id_articulo"] + "'  class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td style='width:60px' align='left'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                        break;
                        case '2':
                            Label4.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td style='width:60px' align='left'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                        break;
                        case '3':
                            Label5.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td style='width:60px' align='left'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                        break;
                    }
                   }
                else
                {
                    switch (controlador)
                    {
                        case '0':
                            Label2.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '1':
                            Label3.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '2':
                            Label4.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '3':
                            Label5.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                    }
                 }
                if(controlador=='3')
                {
                    controlador = '0';
                }
                else
                {
                    controlador++;
                }
            }
           Label2.Text +="</div>";
           Label3.Text += "</div>";
           Label4.Text += "</div>";
           Label5.Text += "</div>";
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlDataReader articulos = null;
        if(TextBox1.Text!="")
        {
            SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE nombre LIKE '%" + TextBox1.Text + "%' ORDER BY nombre", conexion);
            articulos = comando.ExecuteReader();
        }
        else
        {
            SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE Id_categoria='" + DropDownList1.SelectedValue + "' ORDER BY nombre", conexion);
            articulos = comando.ExecuteReader();
        }
        if (articulos.HasRows)
        {
            Label2.Text = "<div id='sortable2' class='contenedorArticulos' >";
            Label3.Text = "<div id='sortable3' class='contenedorArticulos' >";
            Label4.Text = "<div id='sortable4' class='contenedorArticulos' >";
            Label5.Text = "<div id='sortable5' class='contenedorArticulos' >";
            int controlador = '0';
            while (articulos.Read())
            {
                int ex = Convert.ToInt32(articulos["Existencias"]);
                if (ex <= 0)
                {
                    switch (controlador)
                    {
                        case '0':
                            Label2.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '1':
                            Label3.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '2':
                            Label4.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '3':
                            Label5.Text += "<li id='" + articulos["Id_articulo"] + "' class='elementsnull'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                    }
                }
                else
                {
                    switch (controlador)
                    {
                        case '0':
                            Label2.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '1':
                            Label3.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '2':
                            Label4.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                        case '3':
                            Label5.Text += "<li id='" + articulos["Id_articulo"] + "' class='elements'><br><table width='95%' ><tr><td rowspan='4' style='vertical-align:middle;width:50px'><img src='Imagenes/Products/" + articulos["imagen"].ToString() + "' width='50px' heigth='50px'></td></tr><tr><td align='left' style='width:60px'>Nombre:</td><td align='left'>" + articulos["nombre"] + "</td></tr><tr><td align='left'>Cantidad:</td><td align='left'>" + articulos["Existencias"] + "</td></tr><tr><td align='left'>Precio:</td><td align='left'>$" + articulos["precio"] + "</td></tr></table></li>";
                            break;
                    }
                }
                if (controlador == '3')
                {
                    controlador = '0';
                }
                else
                {
                    controlador++;
                }
            }
            Label2.Text += "</div>";
            Label3.Text += "</div>";
            Label4.Text += "</div>";
            Label5.Text += "</div>";
        }
        }

}