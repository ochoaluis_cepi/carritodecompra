﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reg_comp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ID = Request.QueryString["id_articulo"];
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        SqlCommand comando = new SqlCommand("SELECT * FROM articulos WHERE Id_articulo='"+ID+"'",conexion);
        SqlDataReader res = comando.ExecuteReader();
        if(res.Read())
        {
            Label1.Text = "<table align='center' width='250px' ><tr><td rowspan='2' ><img src='Imagenes/Products/" + res["imagen"] + "' width='100px' height='100px'></td><td>Nombre: " + res["nombre"] + "</td></tr><tr><td>Costo Actual: $" + res["costo"] + "</td></tr></table>";
        }
        else
        {
            Response.Redirect("Compras.aspx?error=1");
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Compras.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text != null && TextBox2.Text != null && TextBox1.Text !="" && TextBox2.Text!="")
        {
            string ID = Request.QueryString["id_articulo"];
            string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            SqlConnection conexion = new SqlConnection(s);
            conexion.Open();
            SqlCommand comando = new SqlCommand("INSERT INTO compras_temporal(Id_articulo,costo,existencias,Id_usuario)VALUES('"+ID+"','"+TextBox1.Text+"','"+TextBox2.Text+"','"+Session["ID"]+"')",conexion);
            int res = comando.ExecuteNonQuery();
            if(res==1)
            {
                Response.Redirect("compras.aspx");
            }
            else
            {
                Label2.Text = "<div class='alert alert-danger' ><center>¡ERROR! No se registro</center></div>";
            }
            conexion.Close();

        }
        else
        {
            Label2.Text = "<div class='alert alert-danger'><center>¡ERROR! Introduce Valores Validos</center></div>";
        }
    }
}