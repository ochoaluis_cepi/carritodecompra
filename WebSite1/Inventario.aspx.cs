﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inventario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
  
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
        SqlConnection conexion = new SqlConnection(s);
        conexion.Open();
        string Consulta = "SELECT a.*,p.nombre AS proveedor FROM articulos AS a INNER JOIN proveedores AS p ON a.Id_proveedor=p.Id_proveedor";
        int opc = Convert.ToInt32(DropDownList1.SelectedValue);
        switch (opc)
        {
            case 1:
                Consulta += " ORDER BY Existencias ASC";
                break;
            case 2:
                Consulta += " ORDER BY Existencias DESC ";
                break;
            case 3:
                Consulta += " WHERE Existencias <='0' ORDER BY Existencias DESC ";
                break;
            case 4:
                Consulta += "";
                break;
        }
       /* if(Request.QueryString["filtro_paginacion"] != null)
        {
            int pagina = Convert.ToInt32(Request.QueryString["filtro_paginacion"]);
            if(pagina==1)
            {
                int inicio = pagina;
                int fin = inicio + 20;
                Consulta += " LIMIT " + inicio + " , " + fin + " ";
            }
            else
            {
                int inicio = pagina * 10;
                int fin = inicio + 20;
                Consulta += " LIMIT " + inicio + " , " + fin + " ";
            }
            
        }*/
       
        Label1.Text = "";
        SqlCommand comando = new SqlCommand(Consulta, conexion);
        SqlDataReader result = comando.ExecuteReader();
        string paginador = "";
        int cont_paginador = 0;
        int p = 0;
        if (result.HasRows)
        {
            Label1.Text += "<table class='table' align='center'><tr><td>ID</td><td>Proveedor</td><td>Articulo</td><td>Precio</td><td>Costo</td><td>Imagen</td><td>Existencias</td></tr>";
            while (result.Read())
            {
                Label1.Text += "<tr><td>" + result["Id_articulo"] + "</td><td>" + result["proveedor"] + "</td><td>" + result["nombre"] + "</td><td>$" + result["precio"] + "</td><td>$" + result["Costo"] + "</td><td><img src='Imagenes/Products/" + result["imagen"] + "' height='40px' width='40px'></td><td>" + result["Existencias"] + "</td></tr>";
                if (cont_paginador % 20 == 0)
                {
                    cont_paginador++;
                    p++;
                    paginador += "<a href='Inventario.aspx?filtro_paginacion=" + p + "' >" + p + "</a>";
                }
  
            }
            Label1.Text += "</table>";
            Label1.Text += paginador;
            
        }
        else
        {
            Label1.Text = "<div class='alert alert-danger' ><center>Sin Resultados</center><div>";
        }
        
        conexion.Close();
    }   
}