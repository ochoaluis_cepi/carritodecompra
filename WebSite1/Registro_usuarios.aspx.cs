﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Registro_usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            if (TextBox1.Text != "" && TextBox3.Text != "" && TextBox4.Text != "" && TextBox5.Text != "" && TextBox6.Text != "" && TextBox7.Text != "" && TextBox8.Text != "")
            {
                string s = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
                SqlConnection conexion = new SqlConnection(s);
                conexion.Open();
                SqlCommand comand = new SqlCommand("Select * FROM usuarios WHERE correo='"+TextBox4.Text+"' OR usuario='"+TextBox5.Text+"'",conexion);
                SqlDataReader verificador = comand.ExecuteReader();
                if(verificador.Read())
                {
                    Label1.Text = "<div class='alert alert-danger'><center>¡Error! El Email o Usuario Ya existe</center></div>";
                }
                else
                {
                MD5 md5 = MD5CryptoServiceProvider.Create();
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] stream = null;
                StringBuilder sb = new StringBuilder();
                stream = md5.ComputeHash(encoding.GetBytes(TextBox7.Text));
                for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
                string pass = sb.ToString();
                verificador.Close();
                SqlCommand comando = new SqlCommand("INSERT INTO usuarios(nombre,direccion,telefono,correo,usuario,password,fecha_nacimiento,activo) VALUES('" + TextBox1.Text + "','" + TextBox2.Text + "','" + TextBox3.Text + "','" + TextBox4.Text + "','" + TextBox5.Text + "','" + pass.ToString() + "','" + TextBox8.Text + "','0')", conexion);
                int res = comando.ExecuteNonQuery();
                if (res == 1)
                {
                    comando.CommandText = "SELECT * FROM usuarios WHERE usuario = '" + TextBox5.Text + "' AND password='" + pass.ToString() + "' AND fecha_nacimiento='" + TextBox8.Text + "'";
                    SqlDataReader result = comando.ExecuteReader();
                    if (result.Read())
                    {

                        DateTime fecha = DateTime.Today;
                        Random r = new Random();
                        int aleatorio1 = r.Next();
                        DateTime hoy = fecha.AddDays(3);
                        string fecha_actual = hoy.ToString("dd-MM-yyyy");
                        string ID = result["Id_usuario"].ToString();
                        string msg_correo = result["correo"].ToString();
                        result.Close();
                        comando.CommandText = "INSERT INTO token(Id_usuario,token,caducidad) VALUES('" + ID + "','" + aleatorio1 + "','" + fecha_actual.ToString() + "') ";


                        comando.ExecuteNonQuery();
                        /*-------------------------MENSAJE DE CORREO----------------------*/

                        //Creamos un nuevo Objeto de mensaje
                        System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

                        //Direccion de correo electronico a la que queremos enviar el mensaje
                        mmsg.To.Add(msg_correo);

                        //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

                        //Asunto
                        mmsg.Subject = "Correo de confirmacion";
                        mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

                        //Direccion de correo electronico que queremos que reciba una copia del mensaje
                        //mmsg.Bcc.Add("destinatariocopia@servidordominio.com"); //Opcional

                        //Cuerpo del Mensaje
                        mmsg.Body = "Gracias por Registrarte, As click en el siguiente link para confirmar tu cuenta \n http://localhost:54241/confirmar_registro.aspx?id_usuario= " + ID + "&token=" + aleatorio1.ToString();

                        mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                        mmsg.IsBodyHtml = false; //Si no queremos que se envíe como HTML

                        //Correo electronico desde la que enviamos el mensaje
                        mmsg.From = new System.Net.Mail.MailAddress("cpi_117@hotmail.com");


                        /*-------------------------CLIENTE DE CORREO----------------------*/

                        //Creamos un objeto de cliente de correo
                        System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

                        //Hay que crear las credenciales del correo emisor
                        cliente.Credentials =
                            new System.Net.NetworkCredential("cpi_117@hotmail.com", "quiensabe");

                        //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
                        /*
                        cliente.Port = 587;
                        cliente.EnableSsl = true;
                        */

                        cliente.Host = "smtp.live.com"; //Para Gmail "smtp.gmail.com";


                        /*-------------------------ENVIO DE CORREO----------------------*/

                        try
                        {
                            //Enviamos el mensaje      
                            cliente.Send(mmsg);
                            Label1.Text = "<div class='alert alert-sucess'><center>¡Exitó! Registro Creado y Correo enviado</center> </div>";
                        }
                        catch (System.Net.Mail.SmtpException ex)
                        {
                            Label1.Text = "<div class='alert alert-danger'><center>¡Error! al Enviar el correo</center> </div>";
                            Label1.Text += "<center>confirmar desde <a href='http://localhost:54241/confirmar_registro.aspx?id_usuario=" + ID + "&token=" + aleatorio1.ToString()+"'>Aqui</a></center>";
                        }
                    }
                    else
                    {
                        Label1.Text = "<div class='alert alert-danger'><center>¡Error! al buscar el registro</center> </div>";
                    }
                }
                else
                {
                    Label1.Text = "<div class='alert alert-danger'><center>¡Error! al realizar el registro</center></div>";
                }
                conexion.Close();
            }
            }
            else
            {
                Label1.Text = "<div class='alert alert-danger'><center>¡Error! Hay Campos vacios en el Formulario</center></div>";
            }
            
        }
    }
    protected void TextBox8_TextChanged(object sender, EventArgs e)
    {

    }

}